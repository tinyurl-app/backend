import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Host } from './utils/host';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });
  await app.listen(3000);
  Host.address = await app.getUrl();
  Object.seal(Host);
}
bootstrap();
