import { URL } from 'url';
import { Host } from './host';

export function extractDomainFromUrl(urlString: string) {
  const url = new URL(urlString);
  return url.hostname;
}

export function generateTinyUrl(hash: string) {
  return `${Host.address}/${hash}`;
}

export function validateUrl(urlString: string): boolean {
  try {
    new URL(urlString);
    return true;
  } catch (err) {
    return false;
  }
}
