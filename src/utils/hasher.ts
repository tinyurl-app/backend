import { createHash } from 'crypto';

function getMD5(text: string): string {
  const md5 = createHash('md5');
  return md5.update(text).digest('hex');
}

export function getTinyUrl(text: string) {
  return getMD5(text).substring(0, 5);
}
