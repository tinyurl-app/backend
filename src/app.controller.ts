import {
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Res,
} from '@nestjs/common';
import { Response } from 'express';
import { UrlService } from './url/url.service';

@Controller()
export class AppController {
  constructor(private readonly urlService: UrlService) {}

  @Get('/:hash')
  async getRedirection(@Res() res: Response, @Param('hash') hash: string) {
    if (!hash) {
      throw new HttpException(
        'The requested url is not valid',
        HttpStatus.NOT_ACCEPTABLE,
      );
    }

    const url = await this.urlService.findOne(hash);

    if (!url) {
      throw new HttpException(
        'None of our records match that shortened url',
        HttpStatus.NOT_FOUND,
      );
    }

    res.redirect(url.longUrl);
  }
}
