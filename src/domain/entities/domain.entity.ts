import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type DomainDocument = Domain & Document;

@Schema({
  timestamps: true,
  versionKey: false,
  toJSON: {
    virtuals: true,
    transform: (doc, ret) => {
      delete ret._id;
      delete ret.__v;
      return ret;
    },
  },
})
export class Domain extends Document {
  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  count: number;
}

export const DomainSchema = SchemaFactory.createForClass(Domain);
