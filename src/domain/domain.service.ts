import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateDomainDto } from './dto/create-domain.dto';
import { Domain, DomainDocument } from './entities/domain.entity';

@Injectable()
export class DomainService {
  constructor(
    @InjectModel(Domain.name) private domainModel: Model<DomainDocument>,
  ) {}

  async createOrUpdate(createDomainDto: CreateDomainDto) {
    const domain = await this.domainModel.findOne({
      name: createDomainDto.name,
    });

    if (domain) {
      return this.updateCount(domain);
    }

    return new this.domainModel({
      ...createDomainDto,
      count: 1,
    }).save();
  }

  updateCount(domain: DomainDocument) {
    const lastUpdated: Date = domain.get('updatedAt');
    const last24hs = new Date();
    last24hs.setHours(last24hs.getHours() - 24);

    if (lastUpdated < last24hs) {
      domain.count = 1;
    } else {
      domain.count = domain.count + 1;
    }

    return domain.save();
  }

  mostPopular() {
    const last24hs = new Date();
    last24hs.setHours(last24hs.getHours() - 24);
    return this.domainModel
      .find({
        updatedAt: {
          $gt: last24hs,
        },
      })
      .sort({
        order: 'desc',
      });
  }
}
