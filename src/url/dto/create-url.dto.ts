export class CreateUrlDto {
  tinyUrl: string;
  longUrl: string;
}
