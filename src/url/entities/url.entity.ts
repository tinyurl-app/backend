import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { Domain } from 'src/domain/entities/domain.entity';

export type UrlDocument = Url & Document;

@Schema({
  versionKey: false,
  toJSON: {
    virtuals: true,
    transform: (doc, ret) => {
      delete ret._id;
      delete ret.__v;
      delete ret.domain;
      return ret;
    },
  },
})
export class Url extends Document {
  @Prop({ required: true })
  tinyUrl: string;

  @Prop({ required: true })
  longUrl: string;

  @Prop({
    type: MongooseSchema.Types.ObjectId,
    ref: 'Domain',
    required: true,
  })
  domain: Domain;
}

export const UrlSchema = SchemaFactory.createForClass(Url);
