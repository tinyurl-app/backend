import {
  Body,
  Controller,
  HttpException,
  HttpStatus,
  Post,
} from '@nestjs/common';
import { DomainService } from 'src/domain/domain.service';
import { getTinyUrl as getShortHash } from 'src/utils/hasher';
import {
  generateTinyUrl,
  validateUrl as isValidUrl,
} from 'src/utils/urlHandler';
import { UrlService } from './url.service';

@Controller('/api/url')
export class UrlController {
  constructor(
    private readonly urlService: UrlService,
    private readonly domainService: DomainService,
  ) {}

  @Post()
  async create(@Body('url') url: string) {
    if (!url || !isValidUrl(url)) {
      throw new HttpException(
        'The provided url is not valid',
        HttpStatus.NOT_ACCEPTABLE,
      );
    }

    try {
      const hash = getShortHash(url);
      const found = await this.urlService.findOne(hash);
      const response = () => ({
        longUrl: url,
        tinyUrl: generateTinyUrl(hash),
      });

      if (found) {
        this.domainService.updateCount(found.domain);
        return response();
      } else {
        await this.urlService.create({
          longUrl: url,
          tinyUrl: hash,
        });
        return response();
      }
    } catch (err) {
      throw new HttpException(
        'There was an error while shortening the url',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
