import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { DomainService } from 'src/domain/domain.service';
import { extractDomainFromUrl } from 'src/utils/urlHandler';
import { CreateUrlDto } from './dto/create-url.dto';
import { Url, UrlDocument } from './entities/url.entity';

@Injectable()
export class UrlService {
  constructor(
    @InjectModel(Url.name) private urlModel: Model<UrlDocument>,
    private readonly domainService: DomainService,
  ) {}

  async create(data: CreateUrlDto) {
    const domain = await this.domainService.createOrUpdate({
      name: extractDomainFromUrl(data.longUrl),
    });
    const newUrl = await new this.urlModel({
      ...data,
      domain: domain.id,
    }).save();
    return newUrl;
  }

  async findOne(hash: string) {
    const url = await this.urlModel
      .findOne({
        tinyUrl: hash,
      })
      .populate('domain')
      .exec();

    return url;
  }
}
