import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UrlModule } from './url/url.module';
import { DomainModule } from './domain/domain.module';

const MongoDBConnectionString = 'mongodb://localhost/tinyUrl';

@Module({
  imports: [
    MongooseModule.forRoot(MongoDBConnectionString),
    UrlModule,
    DomainModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
