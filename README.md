# TinyURL assignment

## Description

This is the backend of a url shortener app built solely as an assignment.
Althout it's fully functional, it still laks multiple considerations to serve as productive app and it's solely as a demonstrative Proof of Concept.

Its function is to generate a shorter version of any url and to redirect to the original site when requested.
Also, it provides a list of the most popular domains in the past 24 hs.

[](#implementation)

## Implementation

### Backend

The main idea is to obtain a general hash mechanism that satisfies the following criteria: - It's capable of consistently producing a shorter version (max 6 chars) of an arbitrary valid URL. - It's broad enough to prevent collisions.

Considering those restrictions, the solution implements a MD5 hasher. This guarantees a normal distribution of the codes in its output, made of symbols[a-z][a-z][0-9].

Then, the first 6 chars at taken from that string and used as the identity of the url.
This is stored as the shortened part of the url, along with its original url.
Finally, the user is given the server host address plus the encoded section of the hash representing the long url.

When a shortened-url request gets to the server, it searches for its corresponding record using the encoded part and automatically redirects the requester to the original site.

### Frontend

Its an Angular app that has 2 pages.

- Url: Provides a form for retrieving the shortened version of a url.
- Domains: Shows a list with the most popular domains in the last 24 hs

## Installation

```bash
$ npm install
```

## Preparing environment

This backend relies on a local MongoDB Server running instance.
Please, make sure that the default connection string is valid for your case, else provide the correct one.

This setting is located in the `app.module.ts` file.

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

When running the development server, it defaults to `http://localhost:3000`.

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## PART 1 Answers

- What is a URL shortening system?

  - It's a way of producing consistently shorter versions for urls of any arbitrary length. This way, they are simpler and easier to manipulate.

- What's the main value? Who needs such a system and why?

  - Not only it's great for end users who no longer need to handle infinit urls stuffed with thousands parms and queryParams, but also represents and ease of use for emails with includes external links - because length can represent a hassle for templates-, and for apps that rely on short strings (such as SMS or Twitter comments).

- Describe The main mechanism of work and system components.

  - Refer to [Implementation](#implementation)

- What do you think are the main challenges in implementing and running the system. Try to suggest some ideas for advanced features.

  - As described in the [Implementation](#implementation) section, the main challenge here is to provide a way of consistently generating a shorter version and of the same length for any arbitrary long url.

  - Addtionaly, it should neglect the fact that the service might be affected if users with nefarious purposes start making profit of it. This is, spammers might find this solution ideal to hide their sites from blockers, that in the end could potentially mean that our service would be block (tracing origin requests and redirctions).

  - Some advanced features
    - The system should implement a way of generating the encoded version of the url in a way that its extremely difficult to predict what the sortened version would be without actually requesting for it.
    - Set a time period in which the shortened url will be stored.
    - Implement rate limiters and/or IP blockers to prevent excessive overload.
